# codility-TapeEquilibrium

Given an array of int (ranging from -ve to +ve value) with the minimum number of an element is 2 and the maximum element is 100,000, you need to find the lowest difference between two sets of value (of the total sum of the array)